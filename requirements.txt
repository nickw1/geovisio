Flask==2.2.4
psycopg[pool]==3.*
flasgger==0.9.*
Pillow==9.*
Flask-Cors==3.0.*
fs==2.*
fs-s3fs @ git+https://gitlab.com/geovisio/infra/s3fs.git@5d205e31334644e6eb175e2a78fe4d0a201d0b11 # install s3-s3fs from a fork to be able to add fixes
flask-compress==1.*
tqdm==4.*
xmltodict==0.13.*
requests==2.*
yoyo-migrations==8.*
psycopg-binary==3.*
python-dotenv==0.21.*
authlib==1.2.*
Flask-Executor==1.0.*
geopic-tag-reader==0.1.*
