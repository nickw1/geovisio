import flask
from flask import jsonify

bp = flask.Blueprint("configuration", __name__, url_prefix="/api")


@bp.route("/configuration")
def configuration():
	"""Return instance configuration informations
 
	---
	responses:
		200:
			description: Information about the instance configuration
			schema:
				type: object
				properties:
					auth:
						type: object
						properties:
							user_profile:
								type: object
								properties:
									url:
									type: ['null', string]
							enabled:
								type: boolean
						required: ["enabled"]
				required: ["auth"]
	"""
	
	return jsonify({
		'auth': _auth_configuration(),
	})
 
def _auth_configuration():
    from . import auth
    if auth.oauth_provider is None:
        return {
			'enabled': False
		}
    else:
        return {
			'enabled': True,
			'user_profile': {
				'url': auth.oauth_provider.user_profile_page_url()
			}
		}
