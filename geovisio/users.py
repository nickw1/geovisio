import flask
from . import auth

bp = flask.Blueprint("user", __name__, url_prefix="/api/users")


@bp.route("/me")
@auth.login_required_with_redirect()
def postCollection(account):
	"""Get current logged user informations
	---
	responses:
		200:
			description: Information about the logged account
			schema:
				type: object
				properties:
				  	id:
				  		type: string
				  		format: uuid
				  	name:
				  		type: string
					links:
						type: array
						items:
							type: object
							properties:
								href:
									type: string
								ref:
									type: string
								type:
									type: string
	"""
	response = {
		"id": account.id,
		"name": account.name,
		"links": [
			{
					"rel": "catalog",
					"type": "application/json",
					"href": flask.url_for("user.getCatalog", _external=True)
			}
		]
	}
	return flask.jsonify(response)


@bp.route("/me/catalog/")
@auth.login_required_with_redirect()
def getCatalog(account):
	"""Get current logged user catalog
	---
	responses:
		200:
			description: the Catalog listing all sequences associated to given user
			schema:
				$ref: 'https://schemas.stacspec.org/v1.0.0/catalog-spec/json-schema/catalog.json'
	"""
	return flask.redirect(flask.url_for('stac.getUserCatalog', userId=account.id, _external=True))