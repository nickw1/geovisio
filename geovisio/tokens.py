import flask
from flask import current_app, url_for, request
from dateutil import tz
import psycopg
from psycopg.rows import dict_row
from authlib.jose import jwt
from authlib.jose.errors import BadSignatureError, DecodeError
import logging
import uuid
from . import auth, errors


bp = flask.Blueprint("tokens", __name__, url_prefix="/api")


@bp.route("/users/me/tokens", methods=["GET"])
@auth.login_required_with_redirect()
def list_tokens(account):
    """
    List the tokens of a authenticated user
    ---
    responses:
        200:
            description: the token list
            schema:
                type: array
                items:
                    type: object
                    properties:
                        id:
                            type: string
                        description:
                            type: string
                        generated_at:
                            type: string
                        links:
                            type: array
                            items:
                                type: object
                                properties:
                                    href:
                                        type: string
                                    ref:
                                        type: string
                                    type:
                                        type: string
    """

    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            records = cursor.execute(
                """
                SELECT id, description, generated_at FROM tokens WHERE account_id = %(account)s
            """,
                {"account": account.id},
            ).fetchall()

            tokens = [
                {
                    "id": r["id"],
                    "description": r["description"],
                    "generated_at": r["generated_at"]
                    .astimezone(tz.gettz("UTC"))
                    .isoformat(),
                    "links": [
                        {
                            "rel": "self",
                            "type": "application/json",
                            "href": url_for(
                                "tokens.get_jwt_token", token_id=r["id"], _external=True
                            ),
                        }
                    ],
                }
                for r in records
            ]
            return flask.jsonify(tokens)


@bp.route("/users/me/tokens/<uuid:token_id>", methods=["GET"])
@auth.login_required_with_redirect()
def get_jwt_token(token_id: uuid.UUID, account: auth.Account):
    """
    Get the JWT token corresponding to a token id.

    This token will be needed to authenticate others api calls
    ---
        parameters:
            - name: tokenId
              in: path
              type: string
              description: ID of the token
        responses:
            200:
                description: The token, with it's JWT counterpart.
                schema:
                    type: object
                    properties:
                        id:
                            type: string
                        description:
                            type: string
                        generated_at:
                            type: string
                        jwt_token:
                            type: string
                            description: this jwt_token will be needed to authenticate future queries as Bearer token
    """

    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            # check token existence
            token = cursor.execute(
                """
                SELECT id, description, generated_at FROM tokens WHERE account_id = %(account)s AND id = %(token)s
                """,
                {"account": account.id, "token": token_id},
            ).fetchone()

            if not token:
                raise errors.InvalidAPIUsage(
                    "Impossible to find token", status_code=404
                )

            jwt_token = _generate_jwt_token(token["id"])
            return flask.jsonify(
                {
                    "jwt_token": jwt_token,
                    "id": token["id"],
                    "description": token["description"],
                    "generated_at": token["generated_at"]
                    .astimezone(tz.gettz("UTC"))
                    .isoformat(),
                }
            )


@bp.route("/users/me/tokens/<uuid:token_id>", methods=["DELETE"])
@auth.login_required_with_redirect()
def revoke_token(token_id: uuid.UUID, account: auth.Account):
    """
    Delete a token corresponding to a token id.

    This token will not be usable anymore
    ---
        parameters:
            - name: tokenId
              in: path
              type: string
              description: ID of the token
        responses:
            200:
                description: The token has been correctly deleted
    """

    with psycopg.connect(current_app.config["DB_URL"]) as conn:
        with conn.cursor() as cursor:
            # check token existence
            res = cursor.execute(
                """
                DELETE FROM tokens WHERE account_id = %(account)s AND id = %(token)s
                """,
                {"account": account.id, "token": token_id},
            )
            
            token_deleted = res.rowcount

            if not token_deleted:
                raise errors.InvalidAPIUsage(
                    "Impossible to find token", status_code=404
                )
            conn.commit()
            return flask.jsonify({'message': 'token revoked'}), 200

@bp.route("/auth/tokens/generate", methods=["POST"])
def generate_non_associated_token(description=""):
    """
    Generate a new token, not associated to any user
    
    The response contains the JWT token, and this token can be saved, but won't be usable until someone claims it with /auth/tokens/claims/:id
    
    The response contains the claim route as a link with `rel`=`claim`.
    ---
        parameters:
            - name: description
              in: query
              type: string
              description: optional description of the token
        responses:
            200:
                description: The newly generated token
                schema:
                    type: object
                    properties:
                        id:
                            type: string
                        description:
                            type: string
                        generated_at:
                            type: string
                        jwt_token:
                            type: string
                            description: this jwt_token will be needed to authenticate future queries as Bearer token, but will only work after someone has claimed the token
                        links:
                            type: array
                            items:
                                type: object
                                properties:
                                    href:
                                        type: string
                                    ref:
                                        type: string
                                    type:
                                        type: string
    """
    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            token = cursor.execute(
                "INSERT INTO tokens (description) VALUES (%(description)s) RETURNING *",
                {"description": description},
            ).fetchone()
            if not token:
                raise errors.InternalError("Impossible to generate a new token")

            jwt_token = _generate_jwt_token(token["id"])
            token = {
                    "id": token["id"],
                    "jwt_token": jwt_token,
                    "description": token["description"],
                    "generated_at": token["generated_at"].astimezone(tz.gettz("UTC")).isoformat(),
                    "links": [
                        {
                            "rel": "claim",
                            "type": "application/json",
                            "href": url_for(
                                "tokens.claim_non_associated_token", token_id=token["id"], _external=True
                            ),
                        }
                    ],
                }
            return flask.jsonify(token)
        
@bp.route("/auth/tokens/<uuid:token_id>/claim", methods=["GET"])
@auth.login_required_with_redirect()
def claim_non_associated_token(token_id, account):
    """
    Claim a non associated token
    
    The token will now be associated to the logged user.
    
    Only one user can claim a token
    ---
    responses:
        200:
            description: The token has been correctly associated to the account
    """
    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            token = cursor.execute(
                """
                SELECT account_id FROM tokens WHERE id = %(token)s
                """,
                {"token": token_id},
            ).fetchone()
            if not token:
                raise errors.InvalidAPIUsage("Impossible to find token", status_code=404)
            
            associated_account = token['account_id']
            if associated_account:
                if associated_account != account.id:
                    raise errors.InvalidAPIUsage("Token already claimed by another account", status_code=403)
                else:
                    return flask.jsonify({"message": "token already associated to account"}), 200
                
            cursor.execute(
                "UPDATE tokens SET account_id = %(account)s WHERE id = %(token)s",
                {"account": account.id, "token": token_id},
            )
            conn.commit()
            return "You are now logged in the CLI, you can upload your pictures", 200

def _generate_jwt_token(token_id: uuid.UUID) -> str:
    """
    Generate a JWT token from a token's id.

    The JWT token will be signed but not encrypted.

    This will makes the jwt token openly readable, but only a geovisio instance can issue validly signed tokens
    """
    header = {"alg": "HS256"}
    payload = {
        "iss": "geovisio", # Issuer is optional and not used, but it just tell who issued this token
        "sub": str(token_id),
    }
    secret = current_app.config["SECRET_KEY"]
    
    if not secret:
        raise NoSecretKeyException()
        
    s = jwt.encode(header, payload, secret)

    return str(s, "utf-8")


def _decode_jwt_token(jwt_token: str) -> dict:
    """
    Decode a JWT token
    """
    secret = current_app.config["SECRET_KEY"]
    if not secret:
        raise NoSecretKeyException()
    try:
        return jwt.decode(jwt_token, secret)
    except DecodeError as e:
        logging.error(f'Impossible to decode token: {e}')
        raise InvalidTokenException("Impossible to decode token")


class InvalidTokenException(errors.InvalidAPIUsage):
    def __init__(self, details, status_code=401):
        msg = f"Token not valid"
        super().__init__(msg, status_code=status_code, payload={"details": {"error": details}})


class NoSecretKeyException(errors.InternalError):
    def __init__(self):
        msg = "No SECRET_KEY has been defined for the instance (defined by FLASK_SECRET_KEY environment variable), authentication is not possible. Please contact your instance administrator if this is needed."
        super().__init__(msg)

def get_account_from_jwt_token(jwt_token: str) -> auth.Account:
    """
    Get the account corresponding to a JWT token.

    Parameters
    ----------
    jwt_token : str
            JWT token

    Returns
    -------
    auth.Account
            Corresponding Account

    Raises
    ------
    InvalidTokenException
            If the token is not correctly signed, or if the token is not valid anymore
    """
    try:
        decoded = _decode_jwt_token(jwt_token)
    except BadSignatureError as e:
        logging.exception("invalid signature of jwt token")
        raise InvalidTokenException("JWT token signature does not match")
    token_id = decoded["sub"]

    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            # check token existence
            records = cursor.execute(
                """
                SELECT 
                    t.account_id AS id, a.name, a.oauth_provider, a.oauth_id
                FROM tokens t
                LEFT OUTER JOIN accounts a ON t.account_id = a.id 
                WHERE t.id = %(token)s
            """,
                {"token": token_id},
            ).fetchone()
            if not records:
                raise InvalidTokenException("Token does not exist anymore", status_code=403)
            
            if not records["id"]:
                raise InvalidTokenException("Token not yet claimed, this token cannot be used yet. Either claim this token or generate a new one", status_code=403)
                
            return auth.Account(
                id=records["id"],
                name=records["name"],
                oauth_provider=records["oauth_provider"],
                oauth_id=records["oauth_id"],
            )


def get_default_account_jwt_token() -> str:
    """
    Get the default account JWT token. 
    
    Note: do not expose this method externally, only an instance administrator should be able to get the default account JWT token!
    """
    
    with psycopg.connect(current_app.config["DB_URL"], row_factory=dict_row) as conn:
        with conn.cursor() as cursor:
            # check token existence
            records = cursor.execute(
                """
                SELECT t.id AS id
                FROM tokens t
                JOIN accounts a ON t.account_id = a.id 
                WHERE a.is_default
            """).fetchone()
            if not records:
                raise Exception("Default account has no associated token")
            
            return _generate_jwt_token(records['id'])
