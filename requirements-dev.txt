# Only for dev/testing purposes
coverage==6.*
protobuf>=3.0,<4.21
mapbox-vector-tile==1.2.*
pystac==1.*
pytest==6.*
pytest-datafiles==2.*
pyexiv2==2.*
testcontainers-compose==0.0.1rc1
requests-mock==1.*
