from urllib.parse import quote
import requests
import pytest
from geovisio import create_app
from ..conftest import SEQ_IMGS, dburl, waitForSequence, createSequence, uploadPicture

# mark all tests in the module with the docker marker
pytestmark = [pytest.mark.docker, pytest.mark.skipci]

def _get_minio_bucket_url(minio, bucket, subdir):
	url = quote(minio, safe="")
	return f"s3://geovisio:SOME_VERY_SECRET_KEY@{bucket}/{subdir}?endpoint_url={url}"

@pytest.fixture
def split_storage_app(dburl, minio):
	app = create_app({
	  'TESTING': True,
	  'DB_URL': dburl,
	  'FS_URL': None,
	  'FS_TMP_URL': _get_minio_bucket_url(minio, bucket="panoramax-private", subdir="tmp"),
	  'FS_PERMANENT_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="main-pictures"),
	  'FS_DERIVATES_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="derivates"),
	  'SERVER_NAME': 'localhost:5000',
	  'PICTURE_PROCESS_DERIVATES_STRATEGY': 'ON_DEMAND'
	})
	with app.app_context():
		yield app

  
@SEQ_IMGS
def test_minio_split_storage_upload(split_storage_app, datafiles):
	"""Everything should be ok while uploading pictures is the storage are split across several buckets"""
	with split_storage_app.test_client() as client:
		_check_upload(client, datafiles)
  
def _check_upload(client, datafiles):
	sequence = createSequence(client, "séquence")

	pic_id = uploadPicture(client, sequence, pic=(datafiles / "1.jpg").open("rb"), filename="a_pic.jpg", position=1)

	waitForSequence(client, sequence)

	seq_response = client.get(f"{sequence}/items")
	assert seq_response.status_code < 400
	assert len(seq_response.json['features']) == 1
	pic = seq_response.json['features'][0]
	assert pic['id'] == pic_id

	# by default the pictures are served by the API
	assert pic['assets']['hd']['href'] == f'http://localhost:5000/api/pictures/{pic_id}/hd.jpg'
	assert pic['assets']['sd']['href'] == f'http://localhost:5000/api/pictures/{pic_id}/sd.jpg'
	assert pic['assets']['thumb']['href'] == f'http://localhost:5000/api/pictures/{pic_id}/thumb.jpg'

	assert pic['asset_templates']['tiles']['href'] == f'http://localhost:5000/api/pictures/{pic_id}/tiled/{{TileCol}}_{{TileRow}}.jpg'


@pytest.fixture
def same_storage_app(dburl, minio):
	app = create_app({
	  'TESTING': True,
	  'DB_URL': dburl,
	  'FS_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir=""),
	  'FS_TMP_URL': "",
	  'FS_PERMANENT_URL': "",
	  'FS_DERIVATES_URL': "",
	  'SERVER_NAME': 'localhost:5000',
	  'PICTURE_PROCESS_DERIVATES_STRATEGY': 'ON_DEMAND'
	})
	with app.app_context():
		yield app

  
@SEQ_IMGS
def test_minio_same_storage_upload(same_storage_app, datafiles):
	"""Everything should be ok while uploading pictures is the FS_URL is defined"""
	with same_storage_app.test_client() as client:
		_check_upload(client, datafiles)
  

def test_openFilesystemsFromS3(minio):
	"""Test that the uniq FS_URL parameter works for s3 based storage too"""
	from geovisio import filesystems
	import fs.base
	res = filesystems.openFilesystemsFromConfig({"FS_URL": _get_minio_bucket_url(minio, bucket="panoramax-public", subdir='')})

	assert isinstance(res.tmp, fs.base.FS)
	assert isinstance(res.permanent, fs.base.FS)
	assert isinstance(res.derivates, fs.base.FS)
 
	res.tmp.writetext("test.txt", "test")
	res.permanent.writetext("test.txt", "test")
	res.derivates.writetext("test.txt", "test")


@pytest.fixture
def split_storage_app_with_external_url(dburl, minio):
	app = create_app({
	  'TESTING': True,
	  'DB_URL': dburl,
	  'FS_URL': None,
	  'FS_TMP_URL': _get_minio_bucket_url(minio, bucket="panoramax-private", subdir="tmp"),
	  'FS_PERMANENT_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="main-pictures"),
	  'FS_DERIVATES_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="derivates"),
	  'SERVER_NAME': 'localhost:5000',
	  'PICTURE_PROCESS_DERIVATES_STRATEGY': 'PREPROCESS', # exposing derivates pictures only works if derivates are always generated for the moment
	  'API_PERMANENT_PICTURES_PUBLIC_URL': f'{minio}/panoramax-public/main-pictures',
	  'API_DERIVATES_PICTURES_PUBLIC_URL': f'{minio}/panoramax-public/derivates',
   
	})
	with app.app_context():
		yield app

  
@SEQ_IMGS
def test_external_url_for_pictures(split_storage_app_with_external_url, datafiles, minio):
	"""
 	The API should return url to the s3 pictures directly if asked, and those pictures should be available
 	"""
	with split_storage_app_with_external_url.test_client() as client:
		sequence = createSequence(client, "séquence")

		pic_id = uploadPicture(client, sequence, pic=(datafiles / "1.jpg").open("rb"), filename="a_pic.jpg", position=1)

		waitForSequence(client, sequence)

		seq_response = client.get(f"{sequence}/items")
		assert seq_response.status_code < 400
		assert len(seq_response.json['features']) == 1
		pic = seq_response.json['features'][0]
		assert pic['id'] == pic_id

		def pic_storage_path(picId):
			return f"/{str(picId)[0:2]}/{str(picId)[2:4]}/{str(picId)[4:6]}/{str(picId)[6:8]}/{str(picId)[9:]}"

		assert pic['assets']['hd']['href'] == f'{minio}/panoramax-public/main-pictures{pic_storage_path(pic_id)}.jpg'
		f = requests.get(pic['assets']['hd']['href'])
		f.raise_for_status()
		assert len(f.content) > 0
		assert pic['assets']['sd']['href'] == f'{minio}/panoramax-public/derivates{pic_storage_path(pic_id)}/sd.jpg'
		f = requests.get(pic['assets']['sd']['href'])
		f.raise_for_status()
		assert len(f.content) > 0
		assert pic['assets']['thumb']['href'] == f'{minio}/panoramax-public/derivates{pic_storage_path(pic_id)}/thumb.jpg'
		f = requests.get(pic['assets']['thumb']['href'])
		f.raise_for_status()
		assert len(f.content) > 0

		assert pic['asset_templates']['tiles']['href'] == f'{minio}/panoramax-public/derivates{pic_storage_path(pic_id)}/tiles/{{TileCol}}_{{TileRow}}.jpg'
		# we try to access the first tile
		f = requests.get(pic['asset_templates']['tiles']['href'].replace('{TileCol}', "0").replace('{TileRow}', "0"))
		f.raise_for_status()
		assert len(f.content) > 0
  
		r = client.get(f'/api/pictures/{pic_id}/hd.jpg', follow_redirects=False)
		assert r.status_code == 302
		assert r.location == f'{minio}/panoramax-public/main-pictures{pic_storage_path(pic_id)}.jpg'
  
		r = client.get(f'/api/pictures/{pic_id}/sd.jpg', follow_redirects=False)
		assert r.status_code == 302
		assert r.location == f'{minio}/panoramax-public/derivates{pic_storage_path(pic_id)}/sd.jpg'
		r = client.get(f'/api/pictures/{pic_id}/thumb.jpg', follow_redirects=False)
		assert r.status_code == 302
		assert r.location == f'{minio}/panoramax-public/derivates{pic_storage_path(pic_id)}/thumb.jpg'

@SEQ_IMGS
def test_external_derivates_without_preprocess(minio):
	with pytest.raises(Exception) as e:
		create_app({
			'TESTING': True,
			'DB_URL': dburl,
			'FS_URL': None,
			'FS_TMP_URL': _get_minio_bucket_url(minio, bucket="panoramax-private", subdir="tmp"),
			'FS_PERMANENT_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="main-pictures"),
			'FS_DERIVATES_URL': _get_minio_bucket_url(minio, bucket="panoramax-public", subdir="derivates"),
			'SERVER_NAME': 'localhost:5000',
			'PICTURE_PROCESS_DERIVATES_STRATEGY': 'ON_DEMAND',
			'API_PERMANENT_PICTURES_PUBLIC_URL': f'{minio}/panoramax-public/main-pictures',
			'API_DERIVATES_PICTURES_PUBLIC_URL': f'{minio}/panoramax-public/derivates',
		})
