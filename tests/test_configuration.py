
def test_configuration(client):
	r = client.get("/api/configuration")
	assert r.status_code == 200
	assert r.json == {
     	'auth': {
			'enabled': False
		}
	}