import pytest
import psycopg
import os
import re
from fs import open_fs
import configparser
import time
import contextlib

from geovisio import create_app, runner_pictures, db_migrations, filesystems


@pytest.fixture
def dburl():
	# load test.env file if available
	import dotenv
	val = dotenv.load_dotenv("test.env")

	db = os.environ['DB_URL']

	db_migrations.update_db_schema(db, force=True)
	return db


@pytest.fixture
def fsesUrl(tmp_path):
	fstmp = tmp_path / "tmp"
	fstmp.mkdir()
	fspermanent = tmp_path / "permanent"
	fspermanent.mkdir()
	fsderivates = tmp_path / "derivates"
	fsderivates.mkdir()
	return filesystems.FilesystemsURL(
		tmp="osfs://"+str(fstmp),
		permanent="osfs://"+str(fspermanent),
		derivates="osfs://"+str(fsderivates)
	)


@pytest.fixture
def app(dburl, tmp_path, fsesUrl):
	app = create_app({
	  'TESTING': True,
	  'DB_URL': dburl,
	  'FS_URL': None,
	  'FS_TMP_URL': fsesUrl.tmp,
	  'FS_PERMANENT_URL': fsesUrl.permanent,
	  'FS_DERIVATES_URL': fsesUrl.derivates,
	  'SERVER_NAME': 'localhost:5000',
	  'PICTURE_PROCESS_DERIVATES_STRATEGY': 'ON_DEMAND',
	  'SECRET_KEY': 'a very secret key',
	})
	with app.app_context():
		yield app


@pytest.fixture
def client(app):
	with app.app_context():
		with app.test_client() as client:
			yield client


@pytest.fixture
def runner(app):
	return app.test_cli_runner()


# Code for having at least one sequence in tests
FIXTURE_DIR = os.path.join(
	os.path.dirname(os.path.realpath(__file__)),
	'data'
)

SEQ_IMG = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1.jpg'))
SEQ_IMG_FLAT = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, 'c1.jpg'))

SEQ_IMGS = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, '1.jpg'),
	os.path.join(FIXTURE_DIR, '2.jpg'),
	os.path.join(FIXTURE_DIR, '3.jpg'),
	os.path.join(FIXTURE_DIR, '4.jpg'),
	os.path.join(FIXTURE_DIR, '5.jpg')
)

SEQ_IMGS_FLAT = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, 'b1.jpg'),
	os.path.join(FIXTURE_DIR, 'b2.jpg')
)

SEQ_IMGS_NOHEADING = pytest.mark.datafiles(
	os.path.join(FIXTURE_DIR, 'e1.jpg'),
	os.path.join(FIXTURE_DIR, 'e2.jpg'),
	os.path.join(FIXTURE_DIR, 'e3.jpg'),
	os.path.join(FIXTURE_DIR, 'e4.jpg'),
	os.path.join(FIXTURE_DIR, 'e5.jpg')
)

SEQ_IMG_BLURRED = pytest.mark.datafiles(os.path.join(FIXTURE_DIR, '1_blurred.jpg'))
MOCK_BLUR_API = "https://geovisio-blurring.net"


@pytest.fixture
def initSequence(initSequenceApp):
	def fct2(datafiles, preprocess = True):
		return initSequenceApp(datafiles, preprocess)[0]

	return fct2


@pytest.fixture
def initSequenceApp(tmp_path, dburl, fsesUrl):
	seqPath = tmp_path / "seq1"
	seqPath.mkdir()

	def fct(datafiles, preprocess = True, blur = False):
		twoSeqs = os.path.isfile(datafiles / '1.jpg') and os.path.isfile(datafiles / 'b1.jpg')

		if twoSeqs:
			seq2Path = tmp_path / "seq2"
			seq2Path.mkdir()
			for f in os.listdir(datafiles):
				if f not in ["seq1", "seq2", "1_blurred.jpg", "tmp", "derivates", "permanent"]:
					os.rename(datafiles / f, (seq2Path if f[0:1] == "b" else seqPath) / re.sub("^[a-z]+", "", f))
		else:
			for f in os.listdir(datafiles):
				if f not in ["seq1", "1_blurred.jpg", "tmp", "derivates", "permanent"]:
					os.rename(datafiles / f, seqPath / re.sub("^[a-z]+", "", f))
		app = create_app({
			'TESTING': True,
			'API_BLUR_URL': MOCK_BLUR_API if blur else '',
			'PICTURE_PROCESS_DERIVATES_STRATEGY': 'PREPROCESS' if preprocess else 'ON_DEMAND',
			'DB_URL': dburl,
			'FS_URL': None,
			'FS_TMP_URL': fsesUrl.tmp,
			'FS_PERMANENT_URL': fsesUrl.permanent,
			'FS_DERIVATES_URL': fsesUrl.derivates
		})
		with app.app_context():
			with app.test_client() as client:
				uploadSequence(client, tmp_path / "seq1")
				if twoSeqs:
					uploadSequence(client, tmp_path / "seq2")

				return (client, app)

	return fct


def createSequence(test_client, title) -> str:
	seq = test_client.post("/api/collections",
		data={
			'title': title,
		}
	)
	assert seq.status_code < 400
	return seq.headers["Location"]


def uploadPicture(test_client, sesquence_location, pic, filename, position, isBlurred = False):
	postData = {"position": position, "picture": (pic, filename)}

	if isBlurred:
		postData["isBlurred"] = "true"

	picture_response = test_client.post(
		f"{sesquence_location}/items",
		data=postData,
		content_type='multipart/form-data'
	)
	assert picture_response.status_code < 400
	return picture_response.json['id']


def uploadSequence(test_client, directory, wait=True):
	seq_location = createSequence(test_client, os.path.basename(directory))

	pictures_filenames = sorted([
		f for f in os.listdir(directory)
		if re.search(r'\.jpe?g$', f, re.IGNORECASE)
	])

	for i, p in enumerate(pictures_filenames):
		uploadPicture(test_client, seq_location, open(directory / p, "rb"), p, i+1)

	if wait:
		waitForSequence(test_client, seq_location)


def waitForSequence(test_client, seq_location):
    return waitForSequenceState(
        test_client, 
        seq_location, 
        wanted_state=lambda s: all(p['status'] == "ready" for p in s.json["items"]) and s.json['status'] == 'ready'
    )
 
 
def waitForSequenceState(test_client, seq_location, wanted_state):
	"""
 	Wait for a sequence to have a given state
	`wanted_state` should be a function returning true when the sequence state is the one wanted
  	"""
	timeout = 10
	waiting_time = 0.1
	total_time = 0
	while total_time < timeout:
		s = test_client.get(f"{seq_location}/geovisio_status")

		# we wait for the collection and all its pictures to be ready
		if wanted_state(s):
			return s
		time.sleep(waiting_time)
		total_time += waiting_time
	assert False, "sequence not ready"



@pytest.fixture(autouse=True)
def dbCleanup(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			cursor.execute("""
TRUNCATE TABLE sequences, sequences_pictures, pictures, pictures_to_process CASCADE;
""")


@pytest.fixture()
def defaultAccountID(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("SELECT id from accounts where name = 'Default account'").fetchone()
			assert accountID
			accountID = accountID[0]
	return accountID


@pytest.fixture()
def bobAccountID(dburl):
	with psycopg.connect(dburl) as conn:
		with conn.cursor() as cursor:
			accountID = cursor.execute("SELECT id from accounts WHERE name = 'bob'").fetchone()
			if accountID:
				return accountID[0]
			accountID = cursor.execute("INSERT INTO accounts (name) VALUES ('bob') RETURNING id").fetchone()
			assert accountID
			accountID = accountID[0]
	return accountID

@contextlib.contextmanager
def monkeypatched_function(object, name, patch):
	""" Temporarily monkeypatches an object. """
	pre_patched_value = getattr(object, name)
	setattr(object, name, patch)
	yield object
	setattr(object, name, pre_patched_value)
